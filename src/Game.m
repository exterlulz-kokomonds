/* ----====----====----====----====----====----====----====----====----====----
 Game.m (jeweltoy)
 
 JewelToy is a simple game played against the clock.
 Copyright (C) 2001  Giles Williams
 
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ----====----====----====----====----====----====----====----====----====---- */

/* kokomonds is a fork of JewelToy.
 * repository: http://github.com/exterlulz/kokomonds
 */

// TODO: clean

#import "Game.h"
#import "Gem.h"

// MW...
#import "ScoreBubble.h"

@implementation Game

// TODO: refactor with a designated initializer
- (id)init
{
  self = [super init];
  if (self != nil) {
    gemsFaded = 0;
    for (int i = 0; i < 8; i++) {
      for (int j = 0; j < 8; j++) {
        board[i][j] = [[Gem alloc] init];
      }
    }
    
    // MW
    scoreBubbles= [[NSMutableArray arrayWithCapacity:12] retain];
  }
  
  return self;
}

- (id)initWithImagesFrom:(NSArray *) imageArray
{
  srand([[NSDate date] timeIntervalSince1970]);	// seed by time

  self = [super init];
  if (self != nil) {
    for (int i = 0; i < 8; i++) {
      for (int j = 0; j < 8; j++) {
        // TODO: replace with gem type?
        int r = [self randomGemTypeAt:i:j];
        
        Gem *gem = [[Gem gemWithNumber:r andImage:[imageArray objectAtIndex:r]] retain];
        gem._positionOnBoard  = NSMakePoint(i, j);
        gem._positionOnScreen = NSMakePoint(i * 48, j * 48);
        [gem shake];
        board[i][j] = gem;
        /* TODO: remove, replaced with previous code
         [board[i][j] setPositionOnBoard:i:j];
         [board[i][j] setPositionOnScreen:i*48:j*48];
         [board[i][j] shake];
         */
      }
    }
  }
  
  // MW...
  scoreBubbles= [[NSMutableArray arrayWithCapacity:12] retain];
  
  score = 0;
  gemsFaded = 0;
  bonusMultiplier = 1;
  return self;
}

- (id)initWithSpritesFrom:(NSArray *) spriteArray
{
  srand([[NSDate date] timeIntervalSince1970]);	// seed by time

  self = [super init];
  if (self != nil) {
    for (int i = 0; i < 8; i++) {
      for (int j = 0; j < 8; j++) {
        //int r = (rand() % 3)*2+((i+j)%2);
        // TODO: replace with gem type?
        int r = [self randomGemTypeAt:i :j];
        Gem *gem = [[Gem gemWithNumber:r andSprite:[spriteArray objectAtIndex:r]] retain];
        gem._positionOnBoard = NSMakePoint(i, j);
        gem._positionOnScreen = NSMakePoint(i * 48, j * 48);
        [gem shake];
        board[i][j] = gem;
        /* TODO: remove, replaced with the previous code
         [board[i][j] setPositionOnBoard:i:j];
         [board[i][j] setPositionOnScreen:i*48:j*48];
         [board[i][j] shake];
         */
      }
    }
  }
  
  // MW...
  scoreBubbles= [[NSMutableArray arrayWithCapacity:12] retain];
  
  score = 0;
  gemsFaded = 0;
  bonusMultiplier = 1;
  
  return self;
}

- (void)dealloc
{
  for (int i = 0; i < 8; i++)
    for (int j = 0; j < 8; j++)
      [board[i][j] release];

  // MW...
  [scoreBubbles release];
  
  [super dealloc];
}

- (void)setImagesFrom:(NSArray *) imageArray
{
  for (int i = 0; i < 8; i++) {
    for (int j = 0; j < 8; j++) {
      [board[i][j] setImage:[imageArray objectAtIndex:[board[i][j] gemType]]];
    }
  }
}

- (void)setSpritesFrom:(NSArray *)spriteArray
{
  Gem *gem;
  
  for (int i = 0; i < 8; i++) {
    for (int j = 0; j < 8; j++) {
      gem = board[i][j];
      [gem setSprite:[spriteArray objectAtIndex:[gem gemType]]];
    }
  }
}

- (int) randomGemTypeAt:(int)x :(int)y
{
  int c = (x+y) % 2;
  int r = rand() % 7;
  
  if (c) {
    return (r & 6);	// even
  }
  
  if (r == 6) {
    return 1;	// catch returning 7
  }
  
  return (r | 1); 	// odd
}

- (Gem *) gemAt:(int)x :(int)y {
  return board[x][y];
}

//
// MW...
//
- (NSMutableArray *)scoreBubbles
{
  return scoreBubbles;
}
//
////

- (void) setMuted:(BOOL)value
{
  int i,j;
  muted = value;
  if (muted)
    for (i = 0; i < 8; i++)
      for (j = 0; j < 8; j++)
        [board[i][j] setSoundsTink:NULL Sploink:NULL];
  else
    for (i = 0; i < 8; i++)
      for (j = 0; j < 8; j++)
        [board[i][j] setSoundsTink:[NSSound soundNamed:@"tink"] Sploink:[NSSound soundNamed:@"sploink"]];
}    

- (void)swap:(int)xa :(int)ya and:(int)xb :(int)yb
{
  Gem	*swap = board[xa][ya];
  board[xa][ya] = board[xb][yb];
  board[xa][ya]._positionOnBoard = NSMakePoint(xa, ya);
  board[xb][yb] = swap;
  board[xb][yb]._positionOnBoard = NSMakePoint(xb, yb);
  sxa = xa; sxb = xb; sya = ya; syb = yb;
}

- (void)unswap {
  [self swap:sxa:sya and:sxb:syb];
}

- (BOOL)testForThreeAt:(int) x :(int) y
{
  int	tx,ty,cx,cy;
  int bonus, linebonus, scorePerGem;
  float scorebubble_x = -1.0;
  float scorebubble_y = -1.0;
  BOOL result = NO;
  int	gemtype = [board[x][y] gemType];
  tx = x; ty = y; cx = x; cy = y;
  bonus = 0;
  if (board[x][y].state == GEMSTATE_FADING) {
    result = YES;
  }
  while ((tx > 0)&&([board[tx-1][y] gemType]==gemtype))	tx = tx-1;
  while ((cx < 7)&&([board[cx+1][y] gemType]==gemtype))	cx = cx+1;
  if ((cx-tx) >= 2)
  {
    // horizontal line
    int i,j;
    linebonus= 0;
    scorePerGem = (cx-tx)*5;
    for (i = tx; i <= cx; i++)
    {
      linebonus+= scorePerGem;
      [board[i][y] fade];
      for (j=7; j>y; j--) {
        if (board[i][j].state != GEMSTATE_FADING) {
          [board[i][j] shiver];	//	MW prepare to fall
        }
      }
    }
    // to center scorebubble ...
    scorebubble_x = tx + (cx-tx)/2.0;
    scorebubble_y = y;
    //
    bonus += linebonus;
    result = YES;
  }
  while ((ty > 0)&&([board[x][ty-1] gemType]==gemtype))	ty = ty-1;
  while ((cy < 7)&&([board[x][cy+1] gemType]==gemtype))	cy = cy+1;
  if ((cy-ty) >= 2)
  {
    // vertical line
    int i,j;
    linebonus= 0;
    scorePerGem = (cy-ty)*5;
    for (i = ty; i <= cy; i++) {
      linebonus += scorePerGem;
      [board[x][i] fade];
    }
    
    for (j = 7; j > cy; j--) {
      if (board[x][j].state != GEMSTATE_FADING) {
        [board[x][j] shiver]; //	MW prepare to fall
      }
    }
    
    // to center scorebubble ...
    if (scorebubble_x < 0)	// only if one hasn't been placed already ! (for T and L shapes)
    {
      scorebubble_x = x;
      scorebubble_y = ty + (cy-ty)/2.0;
    }
    else			// select the original gem position
    {
      scorebubble_x = x;
      scorebubble_y = y;
    }
    //
    bonus += linebonus;
    result = YES;
  }
  
  // CASCADE BONUS
  if (cascade >= 1) {
    bonus *= cascade;
  }

  // MW's scorebubble
  if (bonus>0) {
    [scoreBubbles addObject:[ScoreBubble scoreWithValue:bonus*bonusMultiplier
                                                     at:NSMakePoint(scorebubble_x * 48 + 24, scorebubble_y * 48 + 24)
                                               duration:40]];
  }
  
  score += bonus * bonusMultiplier;
  return result;
}

- (BOOL)finalTestForThreeAt:(int)x :(int)y
{
  int	tx,ty,cx,cy;
  BOOL result = NO;
  int	gemtype = [board[x][y] gemType];
  tx = x; ty = y; cx = x; cy = y;
  
  if (board[x][y].state == GEMSTATE_FADING)	return YES;
  
  while ((tx > 0)&&([board[tx-1][y] gemType]==gemtype))	tx = tx-1;
  while ((cx < 7)&&([board[cx+1][y] gemType]==gemtype))	cx = cx+1;
  if ((cx-tx) >= 2)
  {
    // horizontal line
    int i;
    for (i = tx; i <= cx; i++)
      [board[i][y] fade];
    result = YES;
  }
  while ((ty > 0)&&([board[x][ty-1] gemType]==gemtype))	ty = ty-1;
  while ((cy < 7)&&([board[x][cy+1] gemType]==gemtype))	cy = cy+1;
  if ((cy-ty) >= 2)
  {
    // vertical line
    int i;
    for (i = ty; i <= cy; i++)
      [board[x][i] fade];
    result = YES;
  }
  return result;
}

- (BOOL)checkForThreeAt:(int) x :(int) y
{
  int	tx,ty,cx,cy;
  int	gemtype = [board[x][y] gemType];
  tx = x; ty = y; cx = x; cy = y;
  while ((tx > 0)&&([board[tx-1][y] gemType]==gemtype))	tx = tx-1;
  while ((cx < 7)&&([board[cx+1][y] gemType]==gemtype))	cx = cx+1;
  if ((cx-tx) >= 2)
    return YES;
  while ((ty > 0)&&([board[x][ty-1] gemType]==gemtype))	ty = ty-1;
  while ((cy < 7)&&([board[x][cy+1] gemType]==gemtype))	cy = cy+1;
  if ((cy-ty) >= 2)
    return YES;
  return NO;
}

- (BOOL) checkBoardForThrees
{
  BOOL result = NO;
  
  // CASCADE BONUS increase
  cascade++;
  
  for (int i = 0; i < 8; i++) {
    for (int j = 0; j < 8; j++) {
      if (board[i][j].state != GEMSTATE_FADING) {
        result = result | [self testForThreeAt:i:j];
      }
    }
  }
  
  // CASCADE BONUS check for reset
  if (!result) {
    cascade = 1;
  }
  
  return result;
}


- (void)showAllBoardMoves
{
  // horizontal moves
  for (int j = 0; j < 8; j++) {
    for (int i = 0; i < 7; i++) {
      [self swap:i:j and:i+1:j];
      [self finalTestForThreeAt:i:j];
      [self finalTestForThreeAt:i+1:j];
      [self unswap];
    }
  }
  
  // vertical moves
  for (int i = 0; i < 8; i++) {
    for (int j = 0; j < 7; j++) {
      [self swap:i:j and:i:j+1];
      [self finalTestForThreeAt:i:j];
      [self finalTestForThreeAt:i:j+1];
      [self unswap];
    }
  }
  
  // over the entire board, set the animationtime for the marked gems higher
  for (int i = 0; i < 8; i++) {
    for (int j = 0; j < 8; j++) {
      Gem *gem = board[i][j];
      
      if (gem.state == GEMSTATE_FADING) {
        [gem erupt];
        [gem setAnimationCounter:1];
      }
      else {
        [gem erupt];
      }
    }
  }
}

- (BOOL)boardHasMoves
{
  // test every possible move
  int i,j;
  BOOL	result = NO;
  // horizontal moves
  for (j = 0; j < 8; j++)
    for (i = 0; i < 7; i++)
    {
      [self swap:i:j and:i+1:j];
      result = [self checkForThreeAt:i:j] | [self checkForThreeAt:i+1:j];
      [self unswap];
      if (result)
      {
        hintx = i;
        hinty = j;
        return result;
      }
    }
  
  // vertical moves
  for (i = 0; i < 8; i++)
    for (j = 0; j < 7; j++)
    {
      [self swap:i:j and:i:j+1];
      result = [self checkForThreeAt:i:j] | [self checkForThreeAt:i:j+1];
      [self unswap];
      if (result)
      {
        hintx = i;
        hinty = j;
        return result;
      }
    }
  return NO;
}

- (void) removeFadedGemsAndReorganiseWithImagesFrom:(NSArray *) imageArray
{
  int i,j,fades, y;
  for (i = 0; i < 8; i++)
  {
    Gem	*column[8];
    fades = 0;
    y = 0;
    // let non-faded gems fall into place
    for (j = 0; j < 8; j++)
    {
      if (board[i][j].state != GEMSTATE_FADING)
      {
        column[y] = board[i][j];
        if (board[i][j]._positionOnScreen.y > y*48)
          [board[i][j] fall];
        y++;
      }
      else
        fades++;
    }
    // transfer faded gems to top of column
    for (j = 0; j < 8; j++)
    {
      if (board[i][j].state == GEMSTATE_FADING)
      {
        // randomly reassign
        int r = (rand() % 7);
        [board[i][j]	setGemType:r];
        [board[i][j]	setImage:[imageArray objectAtIndex:r]];
        
        column[y] = board[i][j];
        board[i][j]._positionOnScreen = NSMakePoint(i * 48, (7 + fades) * 48);
        [board[i][j] fall];
        y++;
        gemsFaded++;
        fades--;
      }
    }
    // OK, shuffling all done - reorganise column
    for (j = 0; j < 8; j++)
    {
      board[i][j] = column[j];
      board[i][j]._positionOnBoard = NSMakePoint(i, j);
    }
  }
}

- (void) removeFadedGemsAndReorganiseWithSpritesFrom:(NSArray *) spriteArray
{
  int j,fades, y;
  
  for (int i = 0; i < 8; i++) {
    Gem	*column[8];
    fades = 0;
    y = 0;
    
    // let non-faded gems fall into place
    for (j = 0; j < 8; j++) {
      if (board[i][j].state != GEMSTATE_FADING) {
        column[y] = board[i][j];
        if (board[i][j]._positionOnScreen.y > y * 48)
          [board[i][j] fall];
        y++;
      }
      else {
        fades++;
      }
    }
    
    // transfer faded gems to top of column
    for (j = 0; j < 8; j++)
    {
      if (board[i][j].state == GEMSTATE_FADING)
      {
        // randomly reassign
        int r = (rand() % 7);
        [board[i][j]	setGemType:r];
        [board[i][j]	setSprite:[spriteArray objectAtIndex:r]];
        
        column[y] = board[i][j];
        board[i][j]._positionOnScreen = NSMakePoint(i * 48, (7 + fades) * 48);
        [board[i][j] fall];
        y++;
        gemsFaded++;
        fades--;
      }
    }
    // OK, shuffling all done - reorganise column
    for (j = 0; j < 8; j++)
    {
      board[i][j] = column[j];
      board[i][j]._positionOnBoard = NSMakePoint(i, j);
    }
  }
}

- (void)shake
{
  for (int i = 0; i < 8; i++) {
    for (int j = 0; j < 8; j++) {
      [board[i][j] shake];
    }
  }
}

- (void)erupt
{
  if (!muted) {
    [[NSSound soundNamed:@"yes"] play];
  }
  
  for (int i = 0; i < 8; i++) {
    for (int j = 0; j < 8; j++) {
      [board[i][j] erupt];
    }
  }
}

- (void) explodeGameOver
{
  //int i,j;
  if (!muted)	[[NSSound soundNamed:@"explosion"] play];
  /*--
   for (i = 0; i < 8; i++)
   for (j = 0; j < 8; j++)
   [board[i][j] erupt];
   --*/
  [self showAllBoardMoves];	// does a delayed eruption
}

- (void) wholeNewGameWithImagesFrom:(NSArray *) imageArray
{
  srand([[NSDate date] timeIntervalSince1970]);	// seed by time
  
  for (int i = 0; i < 8; i++) {
    for (int j = 0; j < 8; j++) {
      //int r = (rand() % 3)*2+((i+j)%2);
      int r = [self randomGemTypeAt:i:j];
      [board[i][j] setGemType:r];
      [board[i][j] setImage:[imageArray objectAtIndex:r]];
      board[i][j]._positionOnBoard = NSMakePoint(i, j);
      board[i][j]._positionOnScreen = NSMakePoint(i * 48, (15 - j) * 48);
      [board[i][j] fall];
    }
  }
  
  score = 0;
  gemsFaded = 0;
  bonusMultiplier = 1;
}

- (void) wholeNewGameWithSpritesFrom:(NSArray *) spriteArray
{
  srand([[NSDate date] timeIntervalSince1970]);	// seed by time
  
  for (int i = 0; i < 8; i++) {
    for (int j = 0; j < 8; j++) {
      //int r = (rand() % 3)*2+((i+j)%2);
      int r = [self randomGemTypeAt:i:j];
      Gem *gem = board[i][j];
      
      [gem setGemType:r];
      [gem setSprite:[spriteArray objectAtIndex:r]];
      gem._positionOnBoard = NSMakePoint(i, j);
      gem._positionOnScreen = NSMakePoint(i * 48, (15 - j) * 48);
      [gem fall];
    }
  }
  
  score = 0;
  gemsFaded = 0;
  bonusMultiplier = 1;
}

- (NSPoint)	hintPoint {
  return NSMakePoint(hintx * 48,hinty * 48);
}

- (float)collectGemsFaded
{
  float result = (float)gemsFaded;
  gemsFaded = 0;
  return result;
}

- (int)score {
  return score;
}

- (int)bonusMultiplier {
  return bonusMultiplier;
}

- (void)increaseBonusMultiplier {
  bonusMultiplier++;
}

@end
