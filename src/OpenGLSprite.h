//
//  OpenGLSprite.h
//  GL_BotChallenge
//
//  Created by Giles Williams on Fri Jun 21 2002.
//

/* kokomonds is a fork of JewelToy.
 * repository: http://github.com/exterlulz/kokomonds
 */

// TODO: clean

#import <Cocoa/Cocoa.h>
#import <Foundation/Foundation.h>

#include <OpenGL/gl.h>

@interface OpenGLSprite : NSObject
{
  NSData*	textureData;
  GLuint	texName;
  
  NSRect	textureCropRect;
  NSSize	textureSize;
  NSSize	size;
}

// ???: - (id)init;
- (id)initWithImage:(NSImage *)textureImage cropRectangle:(NSRect)cropRect size:(NSSize) spriteSize;
- (void)dealloc;

- (void)blitToX:(float)x Y:(float)y Z:(float)z;
- (void)blitToX:(float)x Y:(float)y Z:(float)z Alpha:(float)a;

- (void)makeTextureFromImage:(NSImage *)texImage cropRectangle:(NSRect)cropRect size:(NSSize)spriteSize;

- (void)replaceTextureFromImage:(NSImage *)texImage cropRectangle:(NSRect)cropRect;
- (void)substituteTextureFromImage:(NSImage *)texImage;

@end
