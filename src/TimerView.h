/* ----====----====----====----====----====----====----====----====----====----
 MyTimerView.h (jeweltoy)
 
 JewelToy is a simple game played against the clock.
 Copyright (C) 2001  Giles Williams
 
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ----====----====----====----====----====----====----====----====----====---- */

/* kokomonds is a fork of JewelToy.
 * repository: http://github.com/exterlulz/kokomonds
 */

// TODO: clean

#import <Cocoa/Cocoa.h>

// TODO: replace with progress bar?
@interface TimerView : NSView
{
  float _meter;
  float _decrement;
  id _target;
  SEL _runOutSelector;
  SEL _runOverSelector;
  NSTimer	*_timer;
  BOOL _isRunning;
  
  NSColor	*_color1;
  NSColor	*_color2;
  NSColor	*_colorOK;
  NSColor	*_backColor;
}

- (id)initWithFrame:(NSRect)frame;
- (void)dealloc;

- (void)drawRect:(NSRect)rect;
- (BOOL)isOpaque;

- (void)setPaused:(BOOL)value;
- (void)incrementMeter:(float)value;
- (void)setDecrement:(float)decrement;
- (void)decrementMeter:(float)value;
- (void)setTimerRunningEvery:(NSTimeInterval)timeInterval
                   decrement:(float)value
                  withTarget:(id)target
                  whenRunOut:(SEL)runOutSelector
                 whenRunOver:(SEL)runOverSelector;
- (void)runTimer;
- (void)setTimer:(float)value;

@end
