/* ----====----====----====----====----====----====----====----====----====----
 MyTimerView.m (jeweltoy)
 
 JewelToy is a simple game played against the clock.
 Copyright (C) 2001  Giles Williams
 
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ----====----====----====----====----====----====----====----====----====---- */

/* kokomonds is a fork of JewelToy.
 * repository: http://github.com/exterlulz/kokomonds
 */

// TODO: clean

#import "TimerView.h"

@implementation TimerView

- (id)initWithFrame:(NSRect)frame
{
  self = [super initWithFrame:frame];
  if (self != nil) {
    _meter	= 0.5;
    
    // TODO: replace color1 with [NSColor redColor], same thing for the rest...
    _color1     = [[NSColor redColor] retain];
    _color2     = [[NSColor yellowColor] retain];
    _colorOK    = [[NSColor greenColor] retain];
    _backColor  = [[NSColor blackColor] retain];
    
    _isRunning = NO;
  }
  
  return self;
}

- (void)dealloc
{
  [_color1 release];
  _color1 = nil;
  
  [_color2 release];
  _color2 = nil;
  
  [_colorOK release];
  _colorOK = nil;
  
  [_backColor release];
  _backColor = nil;
  
  [super dealloc];
}

// drawRect: should be overridden in subclassers of NSView to do necessary
// drawing in order to recreate the the look of the view. It will be called
// to draw the whole view or parts of it (pay attention the rect argument);

- (void)drawRect:(NSRect)rect
{
  #pragma unused (rect)
  
  NSRect dotRect;
  
  [_backColor set];
  NSRectFill([self bounds]);   // Equiv to [[NSBezierPath bezierPathWithRect:[self bounds]] fill]
  
  dotRect.origin.x = 4;
  dotRect.origin.y = 4;
  dotRect.size.width  = _meter * ([self bounds].size.width - 8);
  dotRect.size.height = [self bounds].size.height - 8;
  
  [_colorOK set];

  // another MW change...
  if (_decrement != 0) {
    // TODO: refactor, inverse the ifs and replace if<0.3 with else if"
    if (_meter < 0.3) [_color2 set];
    if (_meter < 0.1) [_color1 set];
  }
  
  NSRectFill(dotRect);   // Equiv to [[NSBezierPath bezierPathWithRect:dotRect] fill]
}

- (BOOL)isOpaque {
  return YES;
}

- (void)setPaused:(BOOL)value {
  _isRunning = !value;
}

- (void)incrementMeter:(float)value
{
  _meter += value;
  if (_meter > 1) {
    _meter = 1;
  }
  
  [self setNeedsDisplay:YES];
}

// TODO: accessor
- (void)setDecrement:(float)decrement {
  _decrement = decrement;
}

- (void)decrementMeter:(float)value
{
  _meter -= value;
  if (_meter < 0) {
    _meter = 0;
  }
  
  [self setNeedsDisplay:YES];
}

- (void)setTimerRunningEvery:(NSTimeInterval)timeInterval
                   decrement:(float)value
                  withTarget:(id)target
                  whenRunOut:(SEL)runOutSelector
                 whenRunOver:(SEL)runOverSelector
{
  _decrement = value;
  _target = target;
  _runOutSelector = runOutSelector;
  _runOverSelector = runOverSelector;
  
  if (_timer) {
    [_timer invalidate];
  }
  _timer = [NSTimer scheduledTimerWithTimeInterval:timeInterval
                                           target:self
                                         selector:@selector(runTimer)
                                         userInfo:self
                                          repeats:YES];
  _isRunning = YES;
}

- (void)runTimer
{
  if (_isRunning) {
    if (_meter == 1) {
      _isRunning = NO;
      [_target performSelector:_runOverSelector];
      return;
    }
    
    [self decrementMeter:_decrement];
    
    // MW change added '&& decrement'
    if (_meter == 0 && _decrement != 0) {
      _isRunning = NO;
      [_target performSelector:_runOutSelector];
      return;
    }
  }
}

- (void)setTimer:(float)value
{
  _isRunning = NO;
  _meter = value;
  
  [self setNeedsDisplay:YES];
}

@end
