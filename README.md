# README

kokomonds is a fork of [JewelToy](http://www.aegidian.org/jeweltoy/).

# Links

- [wiki](http://github.com/exterlulz/kokomonds/wiki)
- [bug tracker](http://github.com/exterlulz/kokomonds/issues)
- [git mirror](http://repo.or.cz/w/exterlulz-kokomonds.git)
